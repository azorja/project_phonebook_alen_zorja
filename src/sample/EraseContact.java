package sample;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EraseContact {
    public static boolean eraseContact(String name, String surname){
        File file = new File("C:\\Users\\Alen\\Documents\\JAVA\\Project_Phonebook_AlenZorja\\phoneBook.txt");
        File temp = new File("C:\\Users\\Alen\\Documents\\JAVA\\Project_Phonebook_AlenZorja\\temp.txt");
        try (Scanner in = new Scanner(file)) {
            String[] s = new String[0];
            while (in.hasNext()) {
                boolean found = false;
                s = in.nextLine().split(":");
                if (s[0].equals(name + " " + surname)) {
                    System.out.println("Found contact: " + name + " " + surname + s[1]);
                    found = true;
                }
                if (!found) {
                    try (PrintWriter printer = new PrintWriter(new FileWriter("temp.txt", true))) {
                        printer.println(s[0] + ":" + s[1]);
                        found = false;
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        if(!file.delete()){
            System.out.println("Couldn't delete the contact");
            return false;
        }
        else{
            boolean result = temp.renameTo(file);
            System.out.println("Successfully deleted contact");
            return true;

        }

    }
}