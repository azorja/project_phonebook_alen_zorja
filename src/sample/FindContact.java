package sample;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FindContact {
    public static String findContact(String name, String surname){
        try (Scanner in = new Scanner(new File("phoneBook.txt"))){
            String[] s = new String[0];
            boolean found = false;
            while(in.hasNext()){
                s = in.nextLine().split(":");
                if(s[0].equals(name + " " + surname)){
                    //return "Found contact: " + name + " " + surname + s[1]
                    System.out.println("Found contact: " + name + " " + surname + s[1]);
                    found = true;
                    return s[1];
                }
            }
            if(!found){
                return "Couldn't find " + name + " " + surname + " in the phonebook";
                //System.out.println("Couldn't find " + name + " " + surname + " in the phonebook");
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}

