package sample;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ListContacts {
    public static String listContacts(){
        try (Scanner in = new Scanner(new File("phoneBook.txt"))){
            String[] s = new String[0];
            String outputString = "";
            while(in.hasNext()){
                s = in.nextLine().split(":");
                //System.out.println(s[0] + ": " + s[1]);
                outputString = outputString + s[0] + ": " + s[1] + "\n";
            }
            return outputString;
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        return "";
    }
}
