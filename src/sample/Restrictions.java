package sample;

public class Restrictions {
    public static boolean provjera(String name, String surname){
        boolean itsALetter = true;
        char[] chars = name.toCharArray();
        char[] chars1 = surname.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c)) {
                itsALetter = false;
            }
        }
        for (char c : chars1) {
            if (!Character.isLetter(c)) {
                itsALetter = false;
            }
        }
        if(itsALetter){
            return true;
        }
        else{
            System.out.println("You didn't follow the search rules");
            return false;
        }
    }
    public static boolean isItANumber(int broj){
        if(broj>0 && broj<6){
            return true;
        }
        else{
            System.out.println("Wrong input,you didn't follow the menu rules");
            return false;
        }
    }
}
