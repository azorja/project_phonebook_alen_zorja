package sample;


public class CallContact {
    public static String callContact(String name, String surname){
        String s = FindContact.findContact(name,surname);
        if(s!=null){
            return "Calling " + name + " " + surname + " (" + s + ")";
            //System.out.println("Calling " + name + " " + surname + " (" + s + ")");
        }
        else{
            return "Couldn't find the contact";
            //System.out.println("Couldn't find the contact");
        }
    }
}
