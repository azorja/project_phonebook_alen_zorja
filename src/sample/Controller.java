package sample;


import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller {
    public Button btnCall;
    public Button btnSave;
    public Button btnFind;
    public Button btnListAll;
    public Button btnErase;
    public TextField txt_input_ime;
    public TextField txt_input_prezime;
    public TextArea txt_area_output;
    public TextField txt_input_broj;

    public void Call_contact(ActionEvent actionEvent) {
        if(Check_input()){
            txt_area_output.setText(CallContact.callContact(txt_input_ime.getText(),txt_input_prezime.getText()));
        }
    }

    public void Save_contact(ActionEvent actionEvent) {
        if(Check_input()){
            if(txt_input_ime.getText()!="" && txt_input_prezime.getText()!="" && txt_input_broj.getText()!=""){
                txt_area_output.setText(SaveContact.saveContact(txt_input_ime.getText(),txt_input_prezime.getText(),txt_input_broj.getText()));
            }
            else{
                txt_area_output.setText("Couldn't save contact");
            }
        }
    }

    public void Find_contact(ActionEvent actionEvent) {
        if(Check_input()){
            txt_area_output.setText(FindContact.findContact(txt_input_ime.getText(),txt_input_prezime.getText()));
        }
    }

    public void ListAll_contacts(ActionEvent actionEvent) {
        txt_area_output.setText(ListContacts.listContacts());
    }

    public void Erase_contact(ActionEvent actionEvent) {
        if(EraseContact.eraseContact(txt_input_ime.getText(),txt_input_prezime.getText())){
            txt_area_output.setText("Contact successfully deleted");
        }
        else{
            txt_area_output.setText("Couldn't delete contact");
        }

    }

    public boolean Check_input(){
        if(Restrictions.provjera(txt_input_ime.getText(),txt_input_prezime.getText())){
            return true;
        }
        else{
            txt_area_output.setText("You didn't follow the search rules");
            return false;
        }
    }
}
