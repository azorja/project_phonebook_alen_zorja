package sample;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SaveContact {
    public static String saveContact(String name, String surname,String phoneNumber){
        try(PrintWriter printer = new PrintWriter(new FileWriter("phoneBook.txt", true))) {

            if(name!="" && surname!="" && name!=null && surname!=null && phoneNumber!=null && phoneNumber!=""){
                printer.println(name + " " + surname + ": " + phoneNumber);
                return "Contact " + name + " " + surname + " saved in the phoneBook";
            }
            else{
                return "";
            }
            //System.out.println("Contact " + name + " " + surname + " saved in the phoneBook");
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        return "";
    }
}